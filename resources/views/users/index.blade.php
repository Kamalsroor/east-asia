@extends('layouts.app')
@section('page_title')
    Users
@endsection
@section('small_title')
    Index
@endsection
@section('content')

<div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                        <a href="{{ route('user.create')}}" >
                            <button type="button" class="btn btn-success btn-rounded w-md m-b-5">
                                <i class="fa fa-plus"></i> Add Users
                            </button>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    @include('flash::message')
                    @if(!empty($users))
                        <div class="table-responsive">
                            <table class="table table-striped table-hover text-center">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>type</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        @php $count = $users->currentPage() * 1; @endphp
                                    @foreach($users as $user)
                                        <tr id="removable{{$user->id}}">
                                            <td>{{$count}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>
                                                @if ($user->type == 3 )
                                                admin
                                                @elseif($user->type == 2 )
                                                Supervisor
                                                @elseif($user->type == 1 )
                                                Users
                                                @endif    
                                            </td>
                                            <td class="text-center"><a href="user/{{$user->id}}/edit"
                                                                        class="btn  btn-success"><i class="fa fa-edit"></i></a>
                                            </td>
                                            <td class="text-center">
                                                <button id="{{$user->id}}" data-token="{{ csrf_token() }}"
                                                        data-route="{{URL::route('user.destroy',$user->id)}}"
                                                        type="button" class="destroy btn btn-danger "><i
                                                            class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                        @php $count ++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif

                </div>
                <div class="panel-footer">
                    <div class="text-center">
                            {!! $users->render() !!}

                            
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop