@extends('layouts.app')
@section('page_title')
    Users
@endsection
@section('small_title')
    Edit
@endsection
@section('content')
<div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                            {!! Form::model($model,[
                                'action'=>['UserController@update',$model->id],
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'PUT'
                                ])!!}
                    </div>
                </div>
                <div class="panel-body">
                        @include('users.form')
                </div>
                <div class="panel-footer">
                        <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                            {!! Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    

@stop