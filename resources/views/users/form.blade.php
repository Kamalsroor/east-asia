@include('partials.validation_errors')
@include('flash::message')

<div class="form-group">
    <label for="name">Name</label>
    {!! Form::text('name',null,[
    'class' => 'form-control'
 ]) !!}
</div>
<div class="form-group">
    <label for="email">Email</label>
    {!! Form::text('email',null,[
    'class' => 'form-control'
 ]) !!}
</div>
<div class="form-group">
    <label for="phone">Phone</label>
    {!! Form::tel('phone',null,[
    'class' => 'form-control'
    ]) !!}
</div>

<div class="form-group">
        <label for="type">User Level</label>
        {!! Form::select('type',
        ['1' => 'User','2'=>'Supervisor','3'=>'Admin']
        ,null, ['class'=>'form-control']) !!}
</div>


<div class="form-group">
    <label for="password">Password</label>
    {!! Form::password('password',[
    'class' => 'form-control'
 ]) !!}
</div>



<div class="form-group">
    <label for="password_confirmation">Password Confirmation</label>
    {!! Form::password('password_confirmation',[
    'class' => 'form-control'
 ]) !!}
</div>
{{-- <div class="form-group">
    <label for="roles_list">رتب المستخدمين</label>
    {!! Form::select('roles_list[]',$roles,null,[
    'class' => 'form-control select2',
    'multiple' => 'multiple',
 ]) !!}
</div> --}}




