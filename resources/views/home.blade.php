@extends('layouts.app')
@section('page_title')
    Home Page
@endsection
@section('small_title')
Application statistics
@endsection
@section('content')
<div class=row>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
            <div class="statistic-box statistic-filled-3">
                <h2><span class=count-number>{{$User}}</span> </h2>
                <div class=small>Users </div>
                <i class="ti-world statistic_icon"></i>
                <div class="sparkline3 text-center"></div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
            <div class="statistic-box statistic-filled-1">
                <h2><span class=count-number>{{$Category}}</span> </h2>
                <div class=small>Categories</div>
                <i class="ti-server statistic_icon"></i>
                <div class="sparkline1 text-center"></div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
            <div class="statistic-box statistic-filled-2">
                <h2><span class=count-number>{{$Product}}</span>  </h2>
                <div class=small>Product</div>
                <i class="ti-user statistic_icon"></i>
                <div class="sparkline2 text-center"></div>
            </div>
        </div>

</div>
@endsection
