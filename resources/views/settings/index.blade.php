@extends('layouts.app')
@section('page_title')
   Settings
@endsection

@section('content')

<div class="row">
      <div class="col-sm-12 col-md-12">
          <div class="panel panel-bd lobidrag">
              <div class="panel-heading">
                  <div class="panel-title">
                        {!! Form::model($model,[
                           'action' => ['SettingController@update',$model->id],
                           'method' => 'put',
                           'files' => true,

                           ]) !!}
                  </div>
              </div>
              <div class="panel-body">
                  @include('partials.validation_errors')
                  @include('flash::message')
                  <div class="form-group">
                     {!! Form::label('images', 'SlidShow') !!}
                        <div class="wrapper-all-images">

                            <div class="box-image">
                                <label for="imgInp-1"><img class="blah" id="blah-1" src="{{asset('uploads/products/'.$model->image1)}}" num="1" alt="your image" /></label>
                                <input type='file'id="imgInp-1"   name="image_1" class="imgInp" num="1" />
                            </div>
                            <div class="box-image">
                                 <label for="imgInp-2"><img class="blah" id="blah-2" src="{{asset('uploads/products/'.$model->image2)}}" num="2" alt="your image" /></label>
                                 <input type='file'id="imgInp-2"   name="image_2" class="imgInp" num="2" />
                             </div>
                             <div class="box-image">
                                 <label for="imgInp-3"><img class="blah" id="blah-3" src="{{asset('uploads/products/'.$model->image3)}}" num="3" alt="your image" /></label>
                                 <input type='file'id="imgInp-3"   name="image_3" class="imgInp" num="3" />
                             </div>
                        
                     
                     </div>
                  </div>
  
                  </div>
                      </div>
              </div>
              <div class="panel-footer">
                      <div class="box-footer">
                              <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                          {!! Form::close()!!}
                  </div>
              </div>
          </div>
      </div>
  </div>
  

@endsection
@section('scripts')
<script src="{{asset('plugins/ezdz/jquery.ezdz.js')}}"></script>

<script>
    $(function () {
        var image_ids = [];
        function readURL(input,num) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            console.log(num);

            reader.onload = function(e) {
                $('#blah-'+num).attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
        }

        $(".imgInp").change(function() {
            var num =$(this).attr('num');
            readURL(this,num);
        });

        $(document).on('click','.box-image label span',function(){
            // console.log($(this).attr('num'));
            var reader = new FileReader();
            var num = $(this).attr('num');
            var image_id = $(this).attr('image_id');
            image_ids.push( image_id );
            $('#imageIs').val(image_ids);
            console.log(num);
            $('#blah-'+num).attr('src', "https://screenshotlayer.com/images/assets/placeholder.png");
            $('#imgInp-'+num).val(''); 
            reader.readAsDataURL(input.files[0]);
        });

        
    // CKEDITOR.replace( 'description' );
    // CKEDITOR.replace( 'description1' );
   
});

</script>
@endsection

@section('stylesheet')
<link href="{{asset('plugins/ezdz/jquery.ezdz.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .blah{
        height: 200px;
        cursor:pointer;
        max-width: 100%;
        width: 100%;
        
    }

    .imgInp{
        display: none !important;
    }
    .imgInp::after{
        content: '';
        height: 100px;
        width: 100px;
        background-color: black
    }
.wrapper-all-images {
    display: flex;
    flex-wrap: wrap;
    border: 4px solid #ddd;
    padding: 15px;
}
.wrapper-all-images .box-image {
    flex: 33.33333333333333%;
    max-width: 33.33333333333333%;
    height: 200px;
    padding: 10px;
}

.wrapper-all-images .box-image label{
    position: relative;
}

.wrapper-all-images .box-image label span{
    position: absolute;
    right: 10px;
    font-size: 20px;
    color: #fff;
    background-color: rgba(0, 0, 0, .3);
    padding: 2px;
    cursor: pointer;
}
.wrapper-all-images .box-image .ezdz-dropzone > div {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    font: bold 20px arial;
    padding: 5px;
}
@media (max-width: 992px) {
    .wrapper-all-images .box-image {
        flex: 50%;
        max-width: 50%;
        height: 150px;
    }
}
</style>
@endsection