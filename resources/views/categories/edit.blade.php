@extends('layouts.app')
@section('page_title')
    Category
@endsection
@section('small_title')
    Edit
@endsection
@section('content')
<div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                            {!! Form::model($model,[
                                'action'=>['CategoryController@update',$model->id],
                                'files' => true,
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'PUT'
                                ])!!}
                    </div>
                </div>
                <div class="panel-body">
                        @include('categories.form')

                        <div class="form-group">
                            {!! Form::label('images', 'Product gallery') !!}
    
                        <div class="wrapper-all-images">
                            <div class="box-image">
                                <label for="imgInp"><span num="">X</span><img class="blah" id="blah" src="{{asset('uploads/cat/'.$model->image)}}"num="" alt="your image" /></label>
                                <input type='file'id="imgInp"   name="images[]" class="imgInp" num="" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                        <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                            {!! Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    

@stop

@section('scripts')
<script src="{{asset('plugins/ezdz/jquery.ezdz.js')}}"></script>
<script>
        $(function () {
            function readURL(input,num) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                console.log(num);
    
                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
            }
    
            $(".imgInp").change(function() {
                var num =$(this).attr('num');
                readURL(this,num);
            });
    
            $(document).on('click','.box-image label span',function(){
                // console.log($(this).attr('num'));
                var reader = new FileReader();
                var num = $(this).attr('num');
                $('#blah').attr('src', "https://screenshotlayer.com/images/assets/placeholder.png");
                $('#imgInp').val(''); 
            });
    
            
        // CKEDITOR.replace( 'description' );
        // CKEDITOR.replace( 'description1' );
       
    });
    
    </script>
    @endsection
    
    @section('stylesheet')
    <link href="{{asset('plugins/ezdz/jquery.ezdz.css')}}" rel="stylesheet" type="text/css"/>
    <style>
        .blah{
            height: 400px;
            cursor:pointer;
            max-width: 100%;
            width: 100%;
        }
    
        .imgInp{
            display: none !important;
        }
        .imgInp::after{
            content: '';
            height: 100px;
            width: 100px;
            background-color: black
        }
    .wrapper-all-images {
        display: flex;
        flex-wrap: wrap;
        border: 4px solid #ddd;
        padding: 15px;
    }
    .wrapper-all-images .box-image {
        flex: 100%;
        width: 100%;
        height: 400px;
        padding: 10px;
    }
    
    .wrapper-all-images .box-image label{
        position: relative;
        width: 100% !important;
    }
    
    .wrapper-all-images .box-image label span{
        position: absolute;
        right: 10px;
        font-size: 20px;
        color: #fff;
        background-color: rgba(0, 0, 0, .3);
        padding: 2px;
        cursor: pointer;
    }

    @media (max-width: 992px) {
        .wrapper-all-images .box-image {
            flex: 50%;
            max-width: 50%;
            height: 150px;
        }
    }
    </style>
@endsection
