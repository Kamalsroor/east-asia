
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>شرق اسيا</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- <link rel="shortcut icon" href="assets/dist/img/ico/favicon.png" type="image/x-icon"> --}}
        <link rel="shortcut icon" href="http://eastasiaeg.com/favicon.ico" />

        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
        <script>
            WebFont.load({
                google: {
                    families: ['Alegreya+Sans:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i', 'Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i', 'Open Sans']
                }
            });
        </script>
        <!-- START GLOBAL MANDATORY STYLE -->
        <link href="{{asset('assets/dist/css/base.css')}}" rel="stylesheet" type="text/css"/>
        <!-- START PAGE LABEL PLUGINS --> 
        <!-- START THEME LAYOUT STYLE -->
        <link href="{{asset('assets/dist/css/component_ui.min.css')}}" rel="stylesheet" type="text/css"/>
        <link id="defaultTheme" href="{{asset('assets/dist/css/skins/skin-dark.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('assets/dist/css/custom.css')}}" rel="stylesheet" type="text/css"/>

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        @yield('stylesheet')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
