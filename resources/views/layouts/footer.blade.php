        <script src="{{asset('assets/plugins/jQuery/jquery-1.12.4.min.js')}}"></script>
        <script src="{{asset('assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js')}}"></script>
        <script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/plugins/metisMenu/metisMenu.min.js')}}"></script>
        <script src="{{asset('assets/plugins/lobipanel/lobipanel.min.js')}}"></script>
        <script src="{{asset('assets/plugins/animsition/js/animsition.min.js')}}"></script>
        <script src="{{asset('assets/plugins/fastclick/fastclick.min.js')}}"></script>
        <script src="{{asset('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
        <!-- START PAGE LABEL PLUGINS -->
        <!-- START THEME LABEL SCRIPT -->
        <script src="{{asset('assets/dist/js/app.min.js')}}"></script>
        <script src="{{asset('assets/dist/js/jQuery.style.switcher.js')}}"></script>
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        <script src="{{asset('js/myJs.js')}}"></script>
        <script>
            @if (session()->has('flash_notification.message'))
            @if(session()->get('flash_notification.level') == "success")
            Swal.fire({
                title: "نجحت العملية!",
                text: "{!! session('flash_notification.message') !!}",
                type: "success",
                confirmButtonText: "حسناً"
            });
            @else
            Swal.fire({
                title: "فشلت العملية!",
                text: "{!! session('flash_notification.message') !!}",
                type: "error",
                confirmButtonText: "حسناً"
            });
            @endif
            @endif
        </script>
        @stack('scripts')
        @yield('scripts')
    </body>
</html>
