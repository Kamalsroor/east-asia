@include ('layouts.header')

<body>
    <div id="wrapper" class="wrapper animsition">
        <!-- Navigation -->
        <nav class="navbar navbar-fixed-top">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="material-icons">apps</i>
                </button>
                <a class="navbar-brand" href="{{ route('home')}}">
                    <img class="main-logo" src="{{asset('assets/dist/img/light-logo.png')}}" id="bg" alt="">
                    <!--<span>AdminPage</span>-->
                </a>
            </div>
            <div class="nav-container">
                <!-- /.navbar-header -->
                <ul class="nav navbar-nav hidden-xs">
                    <li><a id="fullscreen" href="#"><i class="material-icons">fullscreen</i> </a></li>
                    <!-- /.Fullscreen -->
                    {{-- <li><a id="menu-toggle" href="#"><i class="material-icons">apps</i></a></li> --}}
                    <!-- /.Sidebar menu toggle icon -->
         
                </ul>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="material-icons">chat</i>
                            {{-- <span class="label label-danger">9</span> --}}
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                            <li class="rad-dropmenu-header"><a href="#">New Messages</a></li>
                            @forelse ($Mail as $item)
                                <li>
                                    <a href="#" class="rad-content">
                                        <div class="inbox-item">
                                            <div class="inbox-item-img"><img src="{{asset('/dist/img/avatar.png')}}" class="img-circle" alt=""></div>
                                            <strong class="inbox-item-author">{{$item->titel}}</strong>
                                            <span class="inbox-item-date">{{$item->created_at}}</span>
                                            <p class="inbox-item-text">{{$item->user->name}}</p>
                                        </div>
                                    </a>
                                </li>
                            @empty
                                
                            @endforelse
                            
                            
                            <li class="rad-dropmenu-footer"><a href="{{ route('mail.index')}}">View All messages</a></li>
                        </ul> <!-- /.Dropdown-messages -->
                    </li><!-- /.dropdown -->
                    <!-- /.Dropdown -->
                    {{-- <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="material-icons">person_add</i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="profile.html"><i class="ti-user"></i>&nbsp; Profile</a></li>
                            <li><a href="mailbox.html"><i class="ti-email"></i>&nbsp; My Messages</a></li>
                            <li><a href="lockscreen.html"><i class="ti-lock"></i>&nbsp; Lock Screen</a></li>
                            <li><a href="#"><i class="ti-settings"></i>&nbsp; Settings</a></li>
                            <li><a href="login.html"><i class="ti-layout-sidebar-left"></i>&nbsp; Logout</a></li>
                        </ul><!-- /.dropdown-user -->
                    </li><!-- /.Dropdown --> --}}
                    <li class="log_out">
                            <script type="">
                                    function submitSignout() {
                                        document.getElementById('signoutForm').submit();

                                    }
                                </script>
                                {!! Form::open(['method' => 'post', 'url' => url('logout'),'id'=>'signoutForm']) !!}

                                {!! Form::close() !!}

                        <a href="#" onclick="submitSignout()">
                            <i class="material-icons">power_settings_new</i>
                        </a>
                    </li><!-- /.Log out -->
                </ul> <!-- /.navbar-top-links -->
            </div>
        </nav>
        <!-- /.Navigation -->
        <div class="sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-heading "> <span>Main Navigation&nbsp;&nbsp;&nbsp;&nbsp;------</span></li>
                    <li><a href="{{ route('home')}}" class="material-ripple"><i class="material-icons">home</i> Dashboard</a></li>
          
                    <li><a href="{{ route('user.index')}}" class="material-ripple"><i class="material-icons">group</i> Users</a></li>
                    <li><a href="{{ route('categories.index')}}" class="material-ripple"><i class="material-icons">home</i> Category</a></li>
                    <li><a href="{{ route('product.index')}}" class="material-ripple"><i class="material-icons">home</i> Product</a></li>
                    <li><a href="{{ route('slider.index')}}" class="material-ripple"><i class="material-icons">home</i> SlidShow</a></li>
                    <li><a href="{{ route('mail.index')}}" class="material-ripple"><i class="material-icons">home</i> Mails</a></li>
                   
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.Left Sidebar-->
        <div class="side-bar right-bar">
            <div class="">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs right-sidebar-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="material-icons">home</i></a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="material-icons">person_add</i></a></li>
                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><i class="material-icons">chat</i></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade  in active" id="home">
                        <ul id="styleOptions" title="switch styling">
                            <li><b>Dark Skin</b></li>
                            <li><a href="javascript: void(0)" data-theme="skin-blue.min"><img src="{{asset('/dist/img/theme-color/1.jpg')}}" alt=""/></a></li>
                            <li><a href="javascript: void(0)" data-theme="skin-dark.min"><img src="{{asset('/dist/img/theme-color/2.jpg')}}" alt=""/></a></li>
                            <li><a href="javascript: void(0)" data-theme="skin-red-light.min" class="skin-logo"><img src="{{asset('/dist/img/theme-color/5.jpg')}}" alt=""/></a></li>
                            <li><b>Dark Skin sidebar</b></li>
                            <li><a href="javascript: void(0)" data-theme="skin-default.min"><img src="{{asset('/dist/img/theme-color/7.jpg')}}" alt=""/> </a></li>
                            <li><a href="javascript: void(0)" data-theme="skin-red-dark.min"><img src="{{asset('/dist/img/theme-color/6.jpg')}}" alt=""/></a></li>
                            <li><a href="javascript: void(0)" data-theme="skin-dark-1.min"><img src="{{asset('/dist/img/theme-color/8.jpg')}}" alt=""/></a></li>
                            <li><b>Light Skin sidebar</b></li>
                            <li><a href="javascript: void(0)" data-theme="skin-default-light.min" class="skin-logo"><img src="{{asset('/dist/img/theme-color/3.jpg')}}" alt=""/></a></li>
                            <li><a href="javascript: void(0)" data-theme="skin-white.min"><img src="{{asset('/dist/img/theme-color/4.jpg')}}" alt=""/></a> </li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane fade " id="profile">
                        <h3 class="sidebar-heading">Activity</h3>
                        <div class="rad-activity-body">
                            <div class="rad-list-group group">
                                <a href="#" class="rad-list-group-item">
                                    <div class="rad-list-icon bg-red pull-left"><i class="fa fa-phone"></i></div>
                                    <div class="rad-list-content"><strong>Client meeting</strong>
                                        <div class="md-text">Meeting at 10:00 AM</div>
                                    </div>
                                </a>
                                <a href="#" class="rad-list-group-item">
                                    <div class="rad-list-icon bg-yellow pull-left"><i class="fa fa-refresh"></i></div>
                                    <div class="rad-list-content"><strong>Created ticket</strong>
                                        <div class="md-text">Ticket assigned to Dev team</div>
                                    </div>
                                </a>
                                <a href="#" class="rad-list-group-item">
                                    <div class="rad-list-icon bg-primary pull-left"><i class="fa fa-check"></i></div>
                                    <div class="rad-list-content"><strong>Activity completed</strong>
                                        <div class="md-text">Completed the dashboard html</div>
                                    </div>
                                </a>
                                <a href="#" class="rad-list-group-item">
                                    <div class="rad-list-icon bg-green pull-left"><i class="fa fa-envelope"></i></div>
                                    <div class="rad-list-content"><strong>New Invitation</strong>
                                        <div class="md-text">Max has invited you to join Inbox</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- /.sidebar-menu -->
                        <h3 class="sidebar-heading">Tasks Progress</h3>
                        <ul class="sidebar-menu">
                            <li>
                                <a href="#">
                                    <h4 class="subheading">
                                        Task one
                                        <span class="label label-danger pull-right">40%</span>
                                    </h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger progress-bar-striped active" style="width: 40%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <h4 class="subheading">
                                        Task two
                                        <span class="label label-success pull-right">20%</span>
                                    </h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success progress-bar-striped active" style="width: 20%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <h4 class="subheading">
                                        Task Three
                                        <span class="label label-warning pull-right">60%</span>
                                    </h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning progress-bar-striped active" style="width: 60%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <h4 class="subheading">
                                        Task four
                                        <span class="label label-primary pull-right">80%</span>
                                    </h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-primary progress-bar-striped active" style="width: 80%"></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.sidebar-menu -->
                    </div>
                    <div role="tabpanel" class="tab-pane fade " id="messages">
                        <div class="message_widgets">
                            <a href="#">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img src="{{asset('/dist/img/avatar.png')}}" class="img-circle" alt=""></div>
                                    <strong class="inbox-item-author">Naeem Khan</strong>
                                    <span class="inbox-item-date">-13:40 PM</span>
                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                    <span class="profile-status available pull-right"></span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img src="{{asset('/dist/img/avatar2.png')}}" class="img-circle" alt=""></div>
                                    <strong class="inbox-item-author">Sala Uddin</strong>
                                    <span class="inbox-item-date">-13:40 PM</span>
                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                    <span class="profile-status away pull-right"></span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img src="{{asset('/dist/img/avatar3.png')}}" class="img-circle" alt=""></div>
                                    <strong class="inbox-item-author">Mozammel</strong>
                                    <span class="inbox-item-date">-13:40 PM</span>
                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                    <span class="profile-status busy pull-right"></span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img src="{{asset('/dist/img/avatar4.png')}}" class="img-circle" alt=""></div>
                                    <strong class="inbox-item-author">Tanzil</strong>
                                    <span class="inbox-item-date">-13:40 PM</span>
                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                    <span class="profile-status offline pull-right"></span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img src="{{asset('/dist/img/avatar5.png')}}" class="img-circle" alt=""></div>
                                    <strong class="inbox-item-author">Amir Khan</strong>
                                    <span class="inbox-item-date">-13:40 PM</span>
                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                    <span class="profile-status available pull-right"></span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img src="{{asset('/dist/img/avatar.png')}}" class="img-circle" alt=""></div>
                                    <strong class="inbox-item-author">Salman Khan</strong>
                                    <span class="inbox-item-date">-13:40 PM</span>
                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                    <span class="profile-status available pull-right"></span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img src="{{asset('/dist/img/avatar.png')}}" class="img-circle" alt=""></div>
                                    <strong class="inbox-item-author">Tahamina</strong>
                                    <span class="inbox-item-date">-13:40 PM</span>
                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                    <span class="profile-status available pull-right"></span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="inbox-item">
                                    <div class="inbox-item-img"><img src="{{asset('/dist/img/avatar4.png')}}" class="img-circle" alt=""></div>
                                    <strong class="inbox-item-author">Jhon</strong>
                                    <span class="inbox-item-date">-13:40 PM</span>
                                    <p class="inbox-item-text">Hey! there I'm available...</p>
                                    <span class="profile-status offline pull-right"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.Right Sidebar -->
        <!-- /.Navbar  Static Side -->
        <div class="control-sidebar-bg"></div>
        <!-- Page Content -->
        <div id="page-wrapper">
            <!-- main content -->
            <div class="content">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="header-icon"><i class="pe-7s-close"></i></div>
                    <div class="header-title">
                        <h1>@yield('page_title')</h1>
                        <small>@yield('small_title')</small>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('home')}}">Home</a></li>
                            <li class="active">@yield('small_title')</li>
                        </ol>
                    </div>
                </div> <!-- /. Content Header (Page header) -->
                @yield('content')
            </div> <!-- /.main content -->
        </div><!-- /#page-wrapper -->
    </div><!-- /#wrapper -->
    <!-- START CORE PLUGINS -->

<!-- jQuery 3 -->
@include('layouts.footer')
