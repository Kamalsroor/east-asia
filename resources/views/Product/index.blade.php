@extends('layouts.app')
@section('page_title')
    Product
@endsection
@section('small_title')
    Index
@endsection
@section('content')

<div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                        <a href="{{ route('product.create')}}" >
                            <button type="button" class="btn btn-success btn-rounded w-md m-b-5">
                                <i class="fa fa-plus"></i> Add New Product
                            </button>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    @include('flash::message')
                    @if(!empty($records))
                        <div class="table-responsive">
                            <table class="table table-striped table-hover text-center">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        
                                        <th>Name</th>
                                        <th>Price</th>
                                        <th>Wholesale Price</th>
                                        <th>Special Price</th>
                                        <th>Category</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        @php if($records->currentPage() == 1 ){
                                                $count = $records->currentPage() * 1;
                                            }else{
                                                $count = $records->currentPage() * 10 - 9;
                                            }
                                        @endphp
                                    @foreach($records as $record)
                                        <tr id="removable{{$record->id}}">
                                            <td>{{$count}}</td>
                                            <td><a href="{{ route('product.show' ,$record->id )}}">{{$record->title}}</a></td>
                                            <td>{{$record->price}}</td>
                                            <td>{{$record->Wholesale_price}}</td>
                                            <td>{{$record->Special_price}}</td>
                                            <td>{{$record->Category->name}}</td>
                                            <td class="text-center"><a href="{{ route('product.edit' ,$record->id )}}"
                                                                        class="btn  btn-success"><i class="fa fa-edit"></i></a>
                                            </td>
                                            <td class="text-center">
                                                <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                                        data-route="{{URL::route('product.destroy',$record->id)}}"
                                                        type="button" class="destroy btn btn-danger "><i
                                                            class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                        @php $count ++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif

                </div>
                <div class="panel-footer">
                    <div class="text-center">
                            {!! $records->render() !!}

                            
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop