@extends('layouts.app')
@section('page_title')
    Product
@endsection
@section('small_title')
    {{$model->title}}
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
 
                </div>
            </div>
            <div class="panel-body">
            <h1>Product Name : {{$model->title}}</h1>
            <p>Product description : </p>
            <br>
            {!!$model->doscription!!}




            @if($model->price > 0 )
            <h2>price :{{$model->price}}</h2>
            @endif
            @if($model->Wholesale_price > 0 )
            <h2>Wholesale Price :{{$model->Wholesale_price}}</h2>
            @endif
            @if($model->Hoot_deals > 0 )
            <h2>Hoot Deals :{{$model->Hoot_deals}}</h2>
            @endif
            @if($model->Special_price > 0 )
            <h2>Special Price :{{$model->Special_price}}</h2>
            @endif
            <h2>Sub Category :{{$model->Category->name}}</h2>
            <h2>Category :{{$model->Category->parent->name}}</h2>
             @if(count($model->Images) > 0)
             @foreach ($model->Images as $Image)
                 
                <div class="col-md-4">
                    <img src="{{asset('uploads/products/'.$Image->url)}}" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" alt="">
                </div>
            @endforeach
             @endif
            </div>
            <div class="panel-footer">
                </div>
            </div>
        </div>
    </div>
</div>

@stop