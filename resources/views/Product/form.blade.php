@include('partials.validation_errors')
@include('flash::message')

<div class="form-group">
    <label for="title">title</label>
    {!! Form::text('title',null,[
    'class' => 'form-control'
 ]) !!}
</div>


<div class="form-group">
    <label for="description">description</label>
    {!! Form::textarea('doscription',null,[
        'class' => 'form-control'
    ]) !!}
</div>


<div class="form-group">
    <label for="price">price</label>
    {!! Form::number('price',null,[
        'class' => 'form-control',
        'min' => '0',
    ]) !!}
</div>
<div class="form-group">
    <label for="Wholesale_price">Wholesale price</label>
    {!! Form::number('Wholesale_price',null,[
        'class' => 'form-control',
        'min' => '0',
    ]) !!}
</div>
<div class="form-group">
    <label for="Special_price">Special price</label>
    {!! Form::number('Special_price',null,[
        'class' => 'form-control',
        'min' => '0',
    ]) !!}
</div>

<div class="form-group">
    <label for="Hoot_deals">Hoot deals</label>
    {!! Form::number('Hoot_deals',null,[
        'class' => 'form-control',
        'min' => '0',
    ]) !!}
</div>

{{-- <div class="form-group"> --}}

    {{-- {!! Form::select('sub_category_id',$SubCategory,null,[
    'class' => 'form-control',
    'placeholder' => 'select Parent or leave it',
 ]) !!} --}}
{{-- </div> --}}




