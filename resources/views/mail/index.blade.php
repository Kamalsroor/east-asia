@extends('layouts.app')
@section('page_title')
    Mail
@endsection
@section('small_title')
    Index
@endsection
@section('content')

<div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                       
                    </div>
                </div>
                <div class="panel-body">
                    @include('flash::message')
                    @if(!empty($records))
                        <div class="table-responsive">
                            <table class="table table-striped table-hover text-center">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        
                                        <th>Tital</th>
                                        <th>Content</th>
                                        <th>User Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        @php if($records->currentPage() == 1 ){
                                                $count = $records->currentPage() * 1;
                                            }else{
                                                $count = $records->currentPage() * 10 - 9;
                                            }
                                        @endphp
                                    @foreach($records as $record)
                                        <tr id="removable{{$record->id}}">
                                            <td>{{$count}}</td>
                                            <td>{{$record->titel}}</a></td>
                                            <td>{{$record->content}}</td>
                                            <td>{{$record->User->name}}</td>
                                        </tr>
                                        @php $count ++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif

                </div>
                <div class="panel-footer">
                    <div class="text-center">
                            {!! $records->render() !!}

                            
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop