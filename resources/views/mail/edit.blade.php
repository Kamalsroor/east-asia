@extends('layouts.app')
@section('page_title')
Product
@endsection
@section('small_title')
Edit
@endsection
@section('scripts')
<script src="{{asset('plugins/ezdz/jquery.ezdz.js')}}"></script>

<script>
    $(function () {
        var image_ids = [];
        function readURL(input,num) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            console.log(num);

            reader.onload = function(e) {
                $('#blah-'+num).attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
        }

        $(".imgInp").change(function() {
            var num =$(this).attr('num');
            readURL(this,num);
        });

        $(document).on('click','.box-image label span',function(){
            // console.log($(this).attr('num'));
            var reader = new FileReader();
            var num = $(this).attr('num');
            var image_id = $(this).attr('image_id');
            image_ids.push( image_id );
            $('#imageIs').val(image_ids);
            console.log(num);
            $('#blah-'+num).attr('src', "https://screenshotlayer.com/images/assets/placeholder.png");
            $('#imgInp-'+num).val(''); 
            reader.readAsDataURL(input.files[0]);
        });

        
    // CKEDITOR.replace( 'description' );
    // CKEDITOR.replace( 'description1' );
   
});

</script>
@endsection

@section('stylesheet')
<link href="{{asset('plugins/ezdz/jquery.ezdz.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .blah{
        height: 200px;
        cursor:pointer;
        max-width: 100%;
        width: 100%;
        
    }

    .imgInp{
        display: none !important;
    }
    .imgInp::after{
        content: '';
        height: 100px;
        width: 100px;
        background-color: black
    }
.wrapper-all-images {
    display: flex;
    flex-wrap: wrap;
    border: 4px solid #ddd;
    padding: 15px;
}
.wrapper-all-images .box-image {
    flex: 25%;
    max-width: 25%;
    height: 200px;
    padding: 10px;
}

.wrapper-all-images .box-image label{
    position: relative;
}

.wrapper-all-images .box-image label span{
    position: absolute;
    right: 10px;
    font-size: 20px;
    color: #fff;
    background-color: rgba(0, 0, 0, .3);
    padding: 2px;
    cursor: pointer;
}
.wrapper-all-images .box-image .ezdz-dropzone > div {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;
    font: bold 20px arial;
    padding: 5px;
}
@media (max-width: 992px) {
    .wrapper-all-images .box-image {
        flex: 50%;
        max-width: 50%;
        height: 150px;
    }
}
</style>
@endsection
@section('content')
<div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                            {!! Form::model($model,[
                                'action'=>['ProductController@update',$model->id],
                                'files' => true,
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'PUT'
                                ])!!}
                    </div>
                </div>
                <div class="panel-body">
                        @include('Product.form')
                        <div class="form-group">
                            {{-- <label for="Parent">Category</label> --}}

                            <div class="form-group">
                                {!! Form::label('images', 'Product gallery') !!}
                                <input type="hidden" id="imageIs" name="imageIs">
                                {{-- {!! Form::file('image', ['accept' => '.jpeg, .png, .jpg, .svg']) !!} --}}

                                <div class="wrapper-all-images">
                                    @if (count($model->Images) > 0)
                                        
                                    @for($i = 0; $i < count($model->Images); $i++)
                                        <div class="box-image">
                                            <label for="imgInp-{{$i}}"><span image_id="{{$model->Images[$i]->id}}" num="{{$i}}">X</span><img class="blah" id="blah-{{$i}}" src="{{asset('uploads/products/'.$model->Images[$i]->url)}}" num="{{$i}}" alt="your image" /></label>
                                            <input type='file'id="imgInp-{{$i}}"   name="images[]" class="imgInp" num="{{$i}}" />
                                        </div>
                                    @endfor
                                    @for($i = count($model->Images); $i < 12; $i++)
                                        <div class="box-image">
                                            <label for="imgInp-{{$i}}"><span num="{{$i}}">X</span><img class="blah" id="blah-{{$i}}" src="https://screenshotlayer.com/images/assets/placeholder.png" num="{{$i}}" alt="your image" /></label>
                                            <input type='file'id="imgInp-{{$i}}"   name="images[]" class="imgInp" num="{{$i}}" />
                                        </div>
                                    @endfor
                                    @else
                                    @for($i = 0; $i < 12; $i++)
                                        <div class="box-image">
                                            <label for="imgInp-{{$i}}"><span num="{{$i}}">X</span><img class="blah" id="blah-{{$i}}" src="https://screenshotlayer.com/images/assets/placeholder.png" num="{{$i}}" alt="your image" /></label>
                                            <input type='file'id="imgInp-{{$i}}"   name="images[]" class="imgInp" num="{{$i}}" />
                                        </div>
                                    @endfor
                                    @endif

                                </div>
                            </div>

                            {{-- {!! Form::file('images[]',null,[
                            'class' => 'form-control',
                            'multiple' => 'multiple'
                            ]) !!} --}}
                        </div>
                </div>
                <div class="panel-footer">
                        <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                            {!! Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    

@stop