@extends('layouts.app')
@section('page_title')
    العملاء
@endsection
@section('small_title')
    جميع العملاء
@endsection

@section('scripts')
<script src="{{asset('plugins/ezdz/jquery.ezdz.js')}}"></script>
<script>
        $(function () {
            function readURL(input,num) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                console.log(num);
    
                reader.onload = function(e) {
                    $('#blah-'+num).attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
            }
    
            $(".imgInp").change(function() {
                var num =$(this).attr('num');
                readURL(this,num);
            });
    
            $(document).on('click','.box-image label span',function(){
                // console.log($(this).attr('num'));
                var num = $(this).attr('num');
                var route   = $(this).data('route');
                console.log(route);
                
                var token   = $(this).data('token');

                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url     : route,
                            type    : 'post',
                            data    : {_method: 'delete', _token :token},
                            dataType:'json',
                            success : function(data){
                                if(data.status === 0)
                                {
                                    //toastr.error(data.msg)
                                    
                                    Swal.fire("خطأ!", data.message, "error")
                                }else{
                                    Swal.fire(
                                        'Deleted!',
                                        data.message,
                                        'success'
                                    )
                                    $('#blah-'+num).parent().parent().remove();
                                }
                            }
                        });
                    
                    }
                });
                // $('#blah-'+num).attr('src', "https://screenshotlayer.com/images/assets/placeholder.png");
                // $('#imgInp-'+num).val(''); 
            });
       
    });
    
</script>
@endsection


@section('content')

<div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-bd lobidrag">
                <div class="panel-heading">
                    <div class="panel-title">
                        <a href="{{ route('slider.create')}}" >
                            <button type="button" class="btn btn-success btn-rounded w-md m-b-5">
                                <i class="fa fa-plus"></i> اضافة صور جديده
                            </button>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    @include('flash::message')
                    @if(!empty($records))
                    <div class="form-group">
                        {!! Form::label('images', 'Product gallery') !!}
                        <div class="wrapper-all-images">
                            @foreach ($records as $item)
                                <div class="box-image">
                                    <label for="imgInp-{{$item->id}}"><span data-token="{{ csrf_token() }}"
                                    data-route="{{URL::route('slider.destroy',$item->id)}}" num="{{$item->id}}">X</span><img class="blah" id="blah-{{$item->id}}" src="{{asset('uploads/products/'.$item->url)}}" num="" alt="your image" /></label>
                                </div>
                            @endforeach
                            @for($i = 0; $i < 12; $i++)
                            @endfor
                        </div>
                    </div>
                    @endif

                </div>
                <div class="panel-footer">
                    <div class="text-center">
                            {!! $records->render() !!}

                            
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('stylesheet')
<link href="{{asset('plugins/ezdz/jquery.ezdz.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .blah{
        height: 200px;
        cursor:pointer;
        max-width: 100%;
        width: 100%;
        
    }

    .imgInp{
        width: 100%;
    }

.wrapper-all-images {
    display: flex;
    flex-wrap: wrap;
    border: 4px solid #ddd;
    padding: 15px;
}
.wrapper-all-images .box-image {
    flex: 25%;
    max-width: 25%;
    padding: 10px;
}

.wrapper-all-images .box-image label{
    position: relative;
}

.wrapper-all-images .box-image label span{
    position: absolute;
    right: 10px;
    font-size: 20px;
    color: #fff;
    background-color: rgba(0, 0, 0, .3);
    padding: 2px;
    cursor: pointer;
}

@media (max-width: 992px) {
    .wrapper-all-images .box-image {
        flex: 50%;
        max-width: 50%;
        height: 150px;
    }
}
</style>
@endsection