@include('partials.validation_errors')
@include('flash::message')
{{-- 'name', 'city', 'address', 'phone', 'WhatsApp', 'note' --}}
<div class="form-group">
    {!! Form::label('images', 'Product gallery') !!}
    <div class="wrapper-all-images">
        @for($i = 0; $i < 12; $i++)
            <div class="box-image">
                <label for="imgInp-{{$i}}"><span num="{{$i}}">X</span><img class="blah" id="blah-{{$i}}" src="https://screenshotlayer.com/images/assets/placeholder.png" num="{{$i}}" alt="your image" /></label>
                <input type='file'id="imgInp-{{$i}}"   name="images[]" class="imgInp" num="{{$i}}" />
            </div>
        @endfor
    </div>
</div>








