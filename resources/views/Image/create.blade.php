@extends('layouts.app')
@section('page_title')
    العملاء
@endsection
@section('small_title')
    اضافة
@endsection
@section('content')
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-heading">
                <div class="panel-title">
                        {!! Form::model($model,[
                            'action' => 'ImageController@store',
                            'files' => true,
                            ]) !!}
                </div>
            </div>
            <div class="panel-body">
                    @include('Image.form')
            </div>
            <div class="panel-footer">
                    <div class="box-footer">
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </div>
                        {!! Form::close()!!}
                </div>
            </div>
        </div>
    </div>
</div>

@stop




@section('stylesheet')
<link href="{{asset('plugins/ezdz/jquery.ezdz.css')}}" rel="stylesheet" type="text/css"/>
<style>
    .blah{
        height: 200px;
        cursor:pointer;
        max-width: 100%;
        width: 100%;
        
    }

    .imgInp{
        display: none !important;
    }
    .imgInp::after{
        content: '';
        height: 100px;
        width: 100px;
        background-color: black
    }
.wrapper-all-images {
    display: flex;
    flex-wrap: wrap;
    border: 4px solid #ddd;
    padding: 15px;
}
.wrapper-all-images .box-image {
    flex: 25%;
    max-width: 25%;
    padding: 10px;
}

.wrapper-all-images .box-image label{
    position: relative;
}

.wrapper-all-images .box-image label span{
    position: absolute;
    right: 10px;
    font-size: 20px;
    color: #fff;
    background-color: rgba(0, 0, 0, .3);
    padding: 2px;
    cursor: pointer;
}

@media (max-width: 992px) {
    .wrapper-all-images .box-image {
        flex: 50%;
        max-width: 50%;
        height: 150px;
    }
}
</style>
@endsection


@section('scripts')
<script src="{{asset('plugins/ezdz/jquery.ezdz.js')}}"></script>
<script>
        $(function () {
            function readURL(input,num) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#blah-'+num).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $(".imgInp").change(function() {
                var num =$(this).attr('num');
                $('#code-'+num).prop( "disabled", false );
                readURL(this,num);
            });

            $(document).on('click','.box-image label span',function(){
                var reader = new FileReader();
                console.log($('#code-'+num));
                
                var num = $(this).attr('num');
                $('#code-'+num).prop( "disabled", true );
                $('#blah-'+num).attr('src', "https://screenshotlayer.com/images/assets/placeholder.png");
                $('#imgInp-'+num).val(''); 
            });
    });
    
</script>
@endsection