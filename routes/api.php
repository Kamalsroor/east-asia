<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1', 'namespace' => 'Api'],function (){
    Route::post('register','AuthController@register');
    Route::post('login','AuthController@login');
    
    Route::group(['middleware'=> 'auth:api'],function (){
        Route::get('product','MainController@Products');
        Route::get('category','MainController@Category');
        Route::get('slider','MainController@SliderImages');
        Route::get('deals','MainController@ProductsSpecialPrice');
        Route::post('mail','MainController@mail');
        
    });
});



// api/v1/governorates



