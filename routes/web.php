<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 *
 * todo install entrust package
 * todo create roles module
 * todo create users module
 * todo create permissions module
 * todo apply permissions to project actions
 * user m-to-m  roles
 * roles  m-to-m  permissions
 * // spatie users m-to-m  permissions
 *
 */

// Route::group(['namespace' => 'Front'],function (){//,'middleware' => 'auth:client-web'
//     Route::get('/', 'MainController@home');
//     Route::get('client-register', 'AuthController@register');
//     Route::get('about', 'MainController@about');
//     Route::post('toggle-favourite', 'MainController@toggleFavourite')->name('toggle-favourite');
// });
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('key:generate');
    Artisan::call('optimize:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    // return what you want
    return 'تم بنجاح';
});

Route::get('/migrate', function() {
    Artisan::call('migrate:fresh');
    Artisan::call('db:seed');

    // return what you want
    return 'تم بنجاح';
});

Auth::routes();
//Admin panel
Route::group(['middleware'=>['auth','Admin']],function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('categories','CategoryController');
    Route::resource('user','UserController');
    Route::get('category/{id}', 'CategoryController@SubCat')->name('categories.subcat');
    Route::get('subcat/{id}', 'SubCategoryController@SubCat')->name('product.subcat');
    Route::resource('subcategory','SubCategoryController');
    Route::resource('product','ProductController');
    Route::resource('mail','MailController');
    Route::resource('slider','ImageController');
    
});
