<?php

namespace App\Providers;

use App\Model\Settings;
use App\Model\Mail;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Auth;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        // $settings = Settings::first();

        $Mail = Mail::orderBy('id', 'DESC')->get()->take(10);
        view()->share(compact('settings','Mail'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
