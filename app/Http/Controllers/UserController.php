<?php

namespace App\Http\Controllers;

use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function changePassword()
    {
        return view('users.reset-password');
    }

    public function changePasswordSave(Request $request)
    {
        $messages = [
            'old-password' => 'required',
            'password' => 'required|confirmed',
        ];
        $rules = [
            'old-password.required' => 'كلمة السر الحالية مطلوبة',
            'password.required' => 'كلمة السر مطلوبة',
        ];
        $this->validate($request,$messages,$rules);

        $user = Auth::user();

        if (Hash::check($request->input('old-password'), $user->password)) {
            // The passwords match...
            $user->password = bcrypt($request->input('password'));
            $user->save();
            flash()->success('تم تحديث كلمة المرور');
            return view('users.reset-password');
        }else{
            flash()->error('كلمة المرور غير صحيحة');
            return view('users.reset-password');
        }

    }

    public function index()
    {

        $users = User::paginate(10);

        return view('users.index',compact('users'));
    }

    public function create(User $model)
    {
        return view('users.create',compact('model'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
//        dd();
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|unique:users|digits:11',
            'password' => 'required|confirmed',
            'email' => 'email|unique:users|',//required
            'type' => 'required'
            // 'roles_list'  => 'required'
        ]);
        $request->merge(['password' => bcrypt($request->password)]);
        $user = User::create($request->except('roles_list'));
        // $user->roles()->attach($request->input('roles_list'));
        $user->api_token = str_random(60);
        $user->save();
        flash()->success('تم إضافة المستخدم بنجاح');
        return redirect(route('user.index'));
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $model = User::findOrFail($id);
        return view('users.edit',compact('model'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request , $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|unique:users,phone,'.$id.'|digits:11',
            'password' => 'sometimes|nullable|confirmed',
            'email' => 'email|unique:users,email,'.$id,
            'type' => 'required'

        ]);
        $user = User::findOrFail($id);

        // $user->roles()->sync((array) $request->input('roles_list'));
        if (request()->filled('password')) {
            $password = bcrypt($request->password);
        }else{
            $password = $user->password;
        }
        $update = $user->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'type' => $request->type,
            'password' => $password,
            'email' => $request->email,
        ]);

        flash()->success('تم تعديل بيانات المستخدم بنجاح.');
        return redirect(route('user.index'));

    }

    public function destroy($id)
    {
        $record = User::findOrFail($id);

        if (!$record) {
            return response()->json([
                'status'  => 0,
                'message' => 'تعذر الحصول على البيانات'
            ]);
        }

        $record->delete();
        return response()->json([
            'status'  => 1,
            'message' => 'تم الحذف بنجاح',
            'id'      => $id
        ]);
    }
}
