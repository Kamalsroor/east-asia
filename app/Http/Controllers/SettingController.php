<?php

namespace App\Http\Controllers;

use App\Model\Settings;
use Illuminate\Http\Request;
use Image;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Setting $model
     * @return \Illuminate\Http\Response
     */
    public function index(Settings $model)
    {
        if ($model->all()->count() > 0) {
            $model = Settings::find(1);
        }
        // dd($model->image1);
        return view('settings.index', compact('model'));
    }


    public function update(Request $request)
    {
        // $this->validate($request, [
        //     'facebook_url'  => 'url',
        //     'twitter_url'   => 'url',
        //     'instagram_url' => 'url',
        //     'google_url'    => 'url',
        //     'youtube_url'    => 'url',
        //     ]);
        // dd($request);
        if (request()->file('image_1')) {
            // dd('image_1');
            $avatar1 = $request->image_1;
            $profileImgIn = time().'_' . $avatar1->getClientOriginalName();
            Image::make($avatar1)->resize(300, 300)->save( public_path('/uploads/products/' . $profileImgIn));
            Settings::find(1)->update([
                'image1' => $profileImgIn,  
            ]);
        }

        if (request()->file('image_2')) {
            // dd('image_2');

            $avatar1 = $request->image_2;
            $profileImgIn = time().'_' . $avatar1->getClientOriginalName();
            Image::make($avatar1)->resize(300, 300)->save( public_path('/uploads/products/' . $profileImgIn));
            Settings::find(1)->update([
                'image2' => $profileImgIn,  
            ]);
        }

        if (request()->file('image_3')) {
            // dd('image_3');

            $avatar1 = $request->image_3;
            $profileImgIn = time().'_' . $avatar1->getClientOriginalName();
            Image::make($avatar1)->resize(300, 300)->save( public_path('/uploads/products/' . $profileImgIn));
            Settings::find(1)->update([
                'image3' => $profileImgIn,  
            ]);
        }
        flash()->success('تم الحفظ بنجاح');
        return back();
    }

}
