<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Product;
use App\Model\SubCategory;
use Image;

use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     //
    //     $records = Category::paginate(10);
    //     return view('categories.index',compact('records'));
    // }



    public function SubCatEdit($id)
    {
        //
        // $records = SubCategory::where('category_id' , $id)->paginate(10);
        // $Category = Category::find($id);

    }
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function SubCat($id)
    {
        //
        // $records = SubCategory::where('category_id' , $id)->paginate(10);
        // $Category = Category::find($id);
        $records = Product::where('sub_category_id' , $id)->paginate(10);
        return view('Product.index',compact('records'));
        // return view('categories.indexSub',compact('records','Category','id'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request , SubCategory $model)
    {
        $id = $request->id;
        $Category = Category::pluck('name', 'id')->toArray();
        return view('subcategory.create',compact('model','Category','id'));
    }


    public function store(Request $request)
    {
        // dd($request);
        $rules = [
            'name' => 'required',
        ];
        $messages = [
            'name.required' => 'Name is required'
        ];
        $this->validate($request,$rules,$messages);

            $record = new SubCategory;
            if (request()->hasFile('images')) {
                    $avatar = request()->file('images')[0];
                    $profileImgIn = time().'_' . $avatar->getClientOriginalName();
                    Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/products/' . $profileImgIn));
                    $record->image = $profileImgIn;
            }
            $record->name = $request->name;
            $record->category_id = $request->id;
            $record->save();

       
        flash()->success('تــم اضــافة القسم بنجــاح');
        return redirect(route('categories.subcat',$request->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = SubCategory::findOrFail($id);
        $Category = Category::pluck('name', 'id')->toArray();
        return view('subcategory.edit',compact('model','Category'));
    }


    public function update(Request $request, $id)
    {
        $rules = [
                 'name' => 'required'
        ];
        $message = [
                  'name.required' => 'Name is required'
        ];
        $this->validate($request,$rules,$message);
        $record = SubCategory::findOrFail($id);
        // dd($request);
        $profileImgIn = $record->image;
        if (request()->hasFile('images')) {
            # code...D
            // if($request->hasFile('input_img')) {
            $avatar = request()->file('images')[0];
            $profileImgIn = time().'_' . $avatar->getClientOriginalName();
            Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/products/' . $profileImgIn));
        }      
            $record->name = $request->input('name');
            $record->image = $profileImgIn;
            $record->save();
        flash()->success('تم التحديث بنجاح');
        return redirect( route('categories.subcat' ,$record->category_id));
    }

    public function updateSub(Request $request, $id)
    {
        $rules = [
                 'name' => 'required'
        ];
        $message = [
                  'name.required' => 'Name is required'
        ];
        $this->validate($request,$rules,$message);
        $record = Category::findOrFail($id);

        if (request()->filled('Parent')) {
            // dd('Parent');
            $record->delete();
            $record = new SubCategory;
            $record->name = $request->input('name');
            $record->category_id = $request->input('Parent');
            $record->save();
        }else{
            // $record = new Category;
            $record->name = $request->input('name');
            $record->save();
        }
        // $record->update($request->all());
        flash()->success('تم التحديث بنجاح');

        if (request()->filled('Parent')) {
            // dd('Parent');
            dd('test');
            return redirect( route('categories.subcat' ,$record->category_id));
        }else{
            dd('test2');
            return redirect(route('categories.index' ,$record->id ));
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = SubCategory::find($id);
        if (!$record) {
            return response()->json([
                'status'  => 0,
                'message' => 'تعذر الحصول على البيانات'
                ]);
            }
        $Product = Product::where('sub_category_id' , $id)->delete();
        $record->delete();
        return response()->json([
                'status'  => 1,
                'message' => 'تم الحذف بنجاح',
                'id'      => $id
            ]);
    }

}
