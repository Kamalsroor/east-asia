<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ImageSlider;
use Image;

class ImageController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      $records = ImageSlider::paginate(10);
      return view('Image.index',compact('records'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create(ImageSlider $model)
  {
    return view('Image.create',compact('model'));
  }

  /**
   * Store a newly created resource in storage.
   *{{-- 'name', 'city', 'address', 'phone', 'WhatsApp', 'note' --}}
   * @return Response
   */
  public function store(Request $request)
  {

    $rules = [
      'images' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ];
    $messages = [
        'images.required' => 'Name is required'
    ];
    if (request()->file('images')) {
      $Size = range(0,count($request->images)-1);
      $images = array_combine($Size, $request->images);
      for ($i=0; $i < count($images) ; $i++) { 
          $avatar = $images[$i];
          $profileImgIn = time().'_' . $avatar->getClientOriginalExtension();
          // dd($profileImgIn,  $avatar);
          Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/products/' . $profileImgIn));
          $Images = new ImageSlider;
          $Images->url = $profileImgIn;
          $Images->save();
      }
    }
    flash()->success('تــم اضــافة الصور بنجــاح');
    return redirect(route('slider.index'));
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit( $id)
  {
    // dd($model->id);
    $model = Client::findOrFail($id);
    return view('client.edit',compact('model'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request , $id)
  {
    $rules = [
      'name' => 'required',
      'city' => 'required',
      'address' => 'sometimes|nullable',
      'phone' => 'required',
      'WhatsApp' => 'sometimes|nullable',
      'note' => 'sometimes|nullable',
    ];
    $messages = [
        'name.required' => 'Name is required'
    ];
    $this->validate($request,$rules,$messages);
        // $record = new Client;
        $record = Client::findOrFail($id);
        $record->name = $request->input('name');
        $record->city = $request->input('city');
        $record->address = $request->input('address');
        $record->phone = $request->input('phone');
        $record->WhatsApp = $request->input('WhatsApp');
        $record->note = $request->input('note');
        $record->save();
    flash()->success('تــم اضــافة القسم بنجــاح');
    return redirect(route('client.index'));
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $record = ImageSlider::findOrFail($id);
    if (!$record) {
      return response()->json([
          'status'  => 0,
          'message' => 'تعذر الحصول على البيانات'
      ]);
    }
    $record->delete();
    return response()->json([
      'status'  => 1,
      'message' => 'تم الحذف بنجاح',
      'id'      => $id
    ]);
  }
  
}

?>