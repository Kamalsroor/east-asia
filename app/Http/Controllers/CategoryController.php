<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\SubCategory;
use App\Model\Product;
use Image;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $records = Category::paginate(10);
        return view('categories.index',compact('records'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function SubCat($id)
    {
        //
        $records = SubCategory::where('category_id' , $id)->paginate(10);
        $Category = Category::find($id);
        return view('categories.indexSub',compact('records','Category','id'));
    }

    public function SubCatEdit($id)
    {
        //
        // $records = SubCategory::where('category_id' , $id)->paginate(10);
        // $Category = Category::find($id);

        $model = SubCategory::findOrFail($id);
        $Category = Category::pluck('name', 'id')->toArray();
        return view('subcategory.edit',compact('model','Category'));
    }
    

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $model)
    {
        // $Category = Category::pluck('name', 'id')->toArray();
        return view('categories.create',compact('model'));
    }


    public function store(Request $request)
    {
        // dd($request);
        $rules = [
            'name' => 'required',
        ];
        $messages = [
            'name.required' => 'Name is required'
        ];
        $this->validate($request,$rules,$messages);

        
            $record = new Category;
            if (request()->hasFile('images')) {
                $avatar = request()->file('images')[0];
                $profileImgIn = time().'_' . $avatar->getClientOriginalName();
                Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/cat/' . $profileImgIn));
                $record->image = $profileImgIn;
            }
            $record->name = $request->input('name');
            $record->save();
        flash()->success('تــم اضــافة القسم بنجــاح');
        return redirect(route('categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Category::findOrFail($id);
        // dd($model);
        // $Category = Category::pluck('name', 'id')->toArray();
        return view('categories.edit',compact('model'));
    }


    public function update(Request $request, $id)
    {
        $rules = [
                 'name' => 'required'
        ];
        $message = [
                  'name.required' => 'Name is required'
        ];
        $this->validate($request,$rules,$message);
        $record = Category::findOrFail($id);
        $profileImgIn = $record->image;
        // dd($request);
        if (request()->hasFile('images')) {
            # code...D
            // if($request->hasFile('input_img')) {
            $avatar = request()->file('images')[0];
            $profileImgIn = time().'_' . $avatar->getClientOriginalName();
            Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/cat/' . $profileImgIn));
        } 
            $record->name = $request->input('name');
            $record->image = $profileImgIn;
            $record->save();
        // $record->update($request->all());
        flash()->success('تم التحديث بنجاح');
        return redirect(route('categories.index'));
    }

    public function updateSub(Request $request, $id)
    {
        $rules = [
                 'name' => 'required'
        ];
        $message = [
                  'name.required' => 'Name is required'
        ];
        $this->validate($request,$rules,$message);
        $record = Category::findOrFail($id);

        if (request()->filled('Parent')) {
            // dd('Parent');
            $record->delete();
            $record = new SubCategory;
            $record->name = $request->input('name');
            $record->category_id = $request->input('Parent');
            $record->save();
        }else{
            // dd('Not Parent');
            // $record = new Category;
            $record->name = $request->input('name');
            $record->save();
        }
        // $record->update($request->all());
        flash()->success('تم التحديث بنجاح');
        return redirect(route('categories.index'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = Category::find($id);
        if (!$record) {
            return response()->json([
                'status'  => 0,
                'message' => 'تعذر الحصول على البيانات'
            ]);
        }
        $SubCategory = SubCategory::where('category_id' , $id)->get();
        foreach ($SubCategory as $Sub) {
            $Product = Product::where('sub_category_id' , $Sub->id)->delete();
        }
        $SubCategory = SubCategory::where('category_id' , $id)->delete();

        $record->delete();
        return response()->json([
                'status'  => 1,
                'message' => 'تم الحذف بنجاح',
                'id'      => $id
            ]);
    }

}
