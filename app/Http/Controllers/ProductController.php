<?php

namespace App\Http\Controllers;

use App\Model\Product;
use App\Model\SubCategory;
use App\Model\Category;
use App\Model\Images;
use Image;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $records = Product::paginate(10);
        return view('Product.index',compact('records'));
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $model)
    {
        $Categorys = Category::with('Children')->get();
        return view('Product.create',compact('model','Categorys'));
    }


    public function store(Request $request)
    {
        // dd($request);
        $rules = [
            'title' => 'required|string',
            'doscription' => 'required|string',
            'price' => 'required|numeric',
            'Wholesale_price' => 'required|numeric',
            'Special_price' => 'required|numeric',
            'sub_category_id' => 'required',
            'Hoot_deals' => 'nullable|numeric',
            
        ];
        $messages = [
            'title.required' => 'Name is required'
        ];
        $this->validate($request,$rules,$messages);
        // dd($request);
            $record = new Product;
            $record->title = $request->input('title');
            $record->doscription = $request->input('doscription');
            $record->price = $request->input('price');
            $record->Wholesale_price = $request->input('Wholesale_price');
            $record->Special_price = $request->input('Special_price');
            $record->Hoot_deals = $request->input('Hoot_deals');
            $record->sub_category_id = $request->input('sub_category_id');
            $record->save();
            
            if (request()->file('images')) {
                $Size = range(0,count($request->images)-1);
                $images = array_combine($Size, $request->images);
                for ($i=0; $i < count($images) ; $i++) { 
                    # code...
                    $avatar = $images[$i];
                    $profileImgIn = time(). $record->title.'_' . $avatar->getClientOriginalName();
                    Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/products/' . $profileImgIn));
                    $Images = new Images;
                    $Images->url = $profileImgIn;
                    $Images->product_id = $record->id;
                    $Images->save();
                }
            }
        flash()->success('تــم اضــافة القسم بنجــاح');
        return redirect(route('product.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
        $model = Product::with('Images')->findOrFail($id);
        // dd($model->doscription);
        // dd($model);
        return view('Product.show',compact('model'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Product::with('Images')->findOrFail($id);
        // $SubCategory = SubCategory::pluck('name', 'id')->toArray();
        $Categorys = Category::with('Children')->get();

        return view('Product.edit',compact('model','Categorys'));

    }


    public function update(Request $request,  $id)
    {
        $rules = [
            'title' => 'required|string',
            'doscription' => 'required|string',
            'price' => 'required|numeric',
            'Wholesale_price' => 'required|numeric',
            'Special_price' => 'required|numeric',
            'sub_category_id' => 'required',
            'Hoot_deals' => 'nullable|numeric',
        ];
        $message = [
            'title.required' => 'Name is required'
        ];
        $this->validate($request,$rules,$message);
       
        
        $delete_ids = explode(',', $request->imageIs);
        // dd($delete_ids,$request->imageIs);
        Images::destroy($delete_ids);


        // $record = new Product;
        $record = Product::findOrFail($id);

        $record->title = $request->input('title');
        $record->doscription = $request->input('doscription');
        $record->price = $request->input('price');
        $record->Wholesale_price = $request->input('Wholesale_price');
        $record->Special_price = $request->input('Special_price');
        $record->Hoot_deals = $request->input('Hoot_deals');
        $record->sub_category_id = $request->input('sub_category_id');
        
        $record->save();

        if (request()->file('images')) {
            // dd($request->images);
            $Size = range(0,count($request->images)-1);
            $images = array_combine($Size, $request->images);
            $avatars = [];
            $ts = 0;
            for ($i=0; $i < count($images) ; $i++) { 
                # code...
                $avatar = $images[$i];
                $profileImgIn = time(). $record->title.'_' . $avatar->getClientOriginalName();
                $avatars = $profileImgIn;
                Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/products/' . $profileImgIn));
                $Images = new Images;
                $Images->url = $profileImgIn;
                $Images->product_id = $record->id;
                $Images->save();
                $ts++;
            }
        }
        // $record->update($request->all());
        flash()->success('تم التحديث بنجاح');
        return redirect(route('product.index'));
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = Product::find($id);
        if (!$record) {
            return response()->json([
                'status'  => 0,
                'message' => 'تعذر الحصول على البيانات'
            ]);
        }

        $record->delete();
        return response()->json([
                'status'  => 1,
                'message' => 'تم الحذف بنجاح',
                'id'      => $id
            ]);
    }

}
