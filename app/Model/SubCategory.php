<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model 
{

    protected $table = 'sub_categories';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('name', 'category_id','image');

    public function parent()
    {
        return $this->belongsTo('App\Model\Category', 'category_id');
    }

    public function Products()
    {
        return $this->hasMany('App\Model\Product', 'sub_category_id');
    }

}