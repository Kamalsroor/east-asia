<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mail extends Model 
{

    protected $table = 'mails';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('titel', 'content', 'user_id');

    public function User()
    {
        return $this->belongsTo('App\Model\User', 'user_id');
    }

}