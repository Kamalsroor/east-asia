<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
		DB::table('settings')->insert([
            'email' => 'admin@gmail.com',
            'image1' => 'tset1.png',
            'image2' => 'tset2.png',
            'image3' => 'tset3.png',
        ]);

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
            'phone' => '01012316954',
            'type' => 3
        ]);

        DB::table('categories')->insert([
            'name' => 'cat1',
        ]);

        DB::table('categories')->insert([
            'name' => 'cat2',
        ]);
        
        DB::table('categories')->insert([
            'name' => 'cat3',
        ]);
        DB::table('categories')->insert([
            'name' => 'cat4',
        ]);
        DB::table('categories')->insert([
            'name' => 'cat5',
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'Sub1',
            'category_id' => 1
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'Sub2',
            'category_id' => 1
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'Sub3',
            'category_id' => 1
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'Sub4',
            'category_id' => 1
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'Sub1',
            'category_id' => 3
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'Sub2',
            'category_id' => 3
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'Sub3',
            'category_id' => 3
        ]);
        DB::table('sub_categories')->insert([
            'name' => 'Sub4',
            'category_id' => 3
        ]);
        DB::table('products')->insert([
            'title' => 'Products1',
            'doscription' => 'pla pla pla pla pla pla pla pla pla pla pla pla pla pla ',
            'price' => '200',
            'sub_category_id' => 1,
        ]);
        DB::table('products')->insert([
            'title' => 'Products2',
            'doscription' => 'pla pla pla pla pla pla pla pla pla pla pla pla pla pla ',
            'price' => '300',
            'sub_category_id' => 1,
        ]);
        DB::table('products')->insert([
            'title' => 'Products3',
            'doscription' => 'pla pla pla pla pla pla pla pla pla pla pla pla pla pla ',
            'price' => '300',
            'sub_category_id' => 3,
        ]);
        DB::table('products')->insert([
            'title' => 'Products4',
            'doscription' => 'pla pla pla pla pla pla pla pla pla pla pla pla pla pla ',
            'price' => '600',
            'sub_category_id' => 6,
        ]);

        DB::table('images')->insert([
            'product_id' => 1,
            'url' => 'defolde.png',
        ]);
        DB::table('images')->insert([
            'product_id' => 1,
            'url' => 'defolde.png',
        ]);
        DB::table('images')->insert([
            'product_id' => 2,
            'url' => 'defolde.png',
        ]);
        DB::table('images')->insert([
            'product_id' => 3,
            'url' => 'defolde.png',
        ]);
        DB::table('images')->insert([
            'product_id' => 3,
            'url' => 'defolde.png',
        ]);
        DB::table('images')->insert([
            'product_id' => 3,
            'url' => 'defolde.png',
        ]);
        DB::table('images')->insert([
            'product_id' => 4,
            'url' => 'defolde.png',
        ]);
        // $this->call(UsersTableSeeder::class);
    }
}
