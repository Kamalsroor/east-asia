<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('title');
			$table->longText('doscription');
			$table->string('price')->nullable();
			$table->string('Wholesale_price')->nullable();
            $table->string('Hoot_deals')->default(0)->nullable();
			$table->string('Special_price')->nullable();
			$table->integer('sub_category_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('products');
	}
}