<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMailsTable extends Migration {

	public function up()
	{
		Schema::create('mails', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('titel');
			$table->longText('content');
			$table->integer('user_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('mails');
	}
}