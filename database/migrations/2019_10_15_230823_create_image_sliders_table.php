<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImageSlidersTable extends Migration {

	public function up()
	{
		Schema::create('image_sliders', function(Blueprint $table) {
			$table->increments('id');
			$table->string('url');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('image_sliders');
	}
}